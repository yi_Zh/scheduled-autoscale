# ELK-Install

Dieses kurze Skript kann dazu genutzt werden einen ELK-Stack (Elastic search, logstash, kibana) automatisiert auf zu bauen. Der ELK-Stack wird mit Hilfe von metric-beat als Monitor für diese Bachelorarbeit benutzt.

## Konfiguration

Zunächst müssen in der Datei settings.conf die externe IP Adresse, so wie die Endpunkt-Adressen für logstash, elastic search und kibana gesetzt werden.
Ebenfalls lassen sich hier die verwendeten Ports ändern.

## Installation

Das Skript wird mit der Datei setup.sh gestartet und installiert danach automatisch die für den ELK-Stack benötigten Komponenten auf einem Ubuntu-Linux System.
