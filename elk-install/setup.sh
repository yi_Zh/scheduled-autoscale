#!/bin/bash
cd $(dirname $0)
source settings.conf

bash bin/repositories
bash bin/elastic
bash bin/logstash
bash bin/kibana
