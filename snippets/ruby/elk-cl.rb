#!/usr/bin/env ruby

require 'elasticsearch'

client = Elasticsearch::Client.new


begin
response = client.search index: "metricbeat-*", body: { 
query:{
  bool:{
    must:[
     {term: { "beat.hostname":"elk-install" } },
     {range: { "@timestamp":{ gte:"now-1m",lte:"now"}}}]
  }},
aggs:{ idle:{avg:{result_field:"system.cpu.idle.pct"}}
  }
} 

idle=response['aggregations']['idle']['value']
 

if idle > 0.2 then
  puts "CPU is running stable @ #{idle}"
else
  puts "CPU in critical section @ #{idle}"
end

sleep 3
end while true

