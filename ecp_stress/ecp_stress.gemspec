Gem::Specification.new do |s|
  s.name        = 'ecp_stress'
  s.version     = '0.0.1'
  s.licenses    = ['MIT']
  s.summary     = 'Elastic container platform stress simulation module'
  s.description = 'This module uses the ecp_deployer module data structure to simulate stress on cluster machines'
  s.authors     = ['Christian-Friedrich Stüben']
  s.email       = 'christian-friedrich.stueben@stud.fh-luebeck.de'
  s.files       = %w[
                  config/stress.conf
                  config/schedule.json
                  lib/ecp_stress/scheduler/scheduler.rb
                  lib/ecp_stress.rb
                  lib/stress_cli.rb
                  ]
  s.homepage    = 'https://rubygems.org/gems/example'
  s.executables = [ 'ecp_stress' ]
end
