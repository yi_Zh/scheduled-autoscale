require 'thread'
require 'json'
require 'timeout'
require 'net/http'

class Scheduler

  class ScheduleEntry
    attr_reader :machine,:start,:duration
    def initialize(machine, start, duration)
      @machine = machine
      @start = start
      @duration = duration
    end
    def to_s
      "E: #{@machine} start:#{@start}, #{@duration}"
    end
  end

  class StressEntry < ScheduleEntry
    attr_reader :stress_params
    def initialize(machine, start, duration, stress_params)
      super(machine, start, duration)
      @stress_params = stress_params
    end
    def to_s
      "#{self.class} on #{@machine}: Begin: #{@start}"
    end
  end

  class SSHCommandEntry < ScheduleEntry
    attr_reader :command
    def initialize(machine, start, duration, command)
      super(machine,start,duration)
      @command = command
    end
    def to_s
      "#{self.class} on #{@machine}: #{@command} Begin: #{@start}"
    end
  end

  class HttpFloodEntry < ScheduleEntry
    attr_reader :port, :path, :retries, :delay
    def initialize(machine, start, duration, port, path, retries, delay)
      super(machine,start,duration)
      @retries = retries
      @delay = delay
      @port = port
      @path = path
    end
    def to_s
      "#{self.class} #{super.to_s} flooding #{@path} on port #{@port} every #{@delay} seconds"
    end
  end

  attr_accessor :stress_location

  def initialize(cluster,credentials)
    @cluster = cluster
    @credentials = credentials
    @config_hash = {'ssh-ip'.to_s=>'private', 'key-pair'.to_s=>'~/.ssh/deployer-key.pem'}
  end

  def parse_schedule(json_file)
    schedule = []
    schedule_hash = JSON.parse(File.read(json_file))
    schedule_hash.each do |entry|
      if stress_entry?(entry)
        schedule.push(StressEntry.new(entry['machine'], entry['start'], entry['duration'], entry['stress_parameter']))
      elsif command_entry?(entry)
        schedule.push(SSHCommandEntry.new(entry['machine'], entry['start'], entry['duration'], entry['command']))
      elsif http_flood_entry?(entry)
        schedule.push(create_http_flood_entry(entry))
      else
        raise IOError.new('Unknown schedule entry')
      end
    end
    schedule.sort{|x,y| x.start-y.start}
  end

  def create_http_flood_entry(entry)
    flood = entry['http_flood']
    port = flood['port'] unless flood.nil?
    path = flood['path'] unless flood.nil?
    retries = flood['retries'] unless flood.nil?
    delay = flood['delay'] unless flood.nil?

    HttpFloodEntry.new(entry['machine'],entry['start'], entry['duration'], port, path, retries, delay)
  end

  def http_flood_entry?(entry)
    !entry['http_flood'].nil?
  end

  def command_entry?(entry)
    !entry['command'].nil?
  end

  def stress_entry?(entry)
    !entry['stress_parameter'].nil?
  end

  def start(stress_json)
    raise IOError.new('No stress location set') if @stress_location.nil?
    raise IOError.new('No stress schedule') if stress_json.nil?

    schedule = parse_schedule(stress_json)

    running = true
    counter = 0
    while running

      while !schedule.empty? and schedule.first.start==counter
        entry = schedule.shift
        fire_entry(entry)

      end

      running = false if schedule.empty?
      sleep 1
      counter +=1
    end
  end

  def fire_entry(entry)
    Utils::LOG.debug(self){"Fired: #{entry}"}
    if entry.instance_of?(StressEntry)
      run_stress(entry)
    elsif entry.instance_of?(SSHCommandEntry)
      run_ssh_command(entry)
    elsif entry.instance_of?(HttpFloodEntry)
      run_http_flood(entry)
    else
      raise IOError.new("Unknown schedule entry #{entry}")
    end
  end

  def run_http_flood(entry)

    ip = @cluster.machines.select{|m| m.hostname==entry.machine}.first.private_ipv4
    uri = URI.parse("http://#{ip}:#{entry.port}")
    http = Net::HTTP.new(uri.host,uri.port)
    http.send_request('GET', "#{entry.path}")

    Timeout::timeout(entry.duration) do
      entry.retries.times do
        header = {:'Content-Type' => 'application/json'}

        # Generate network load data
        data = '{'
        4096.times do |i|
          data+= "\"#{i}\":\"dummy-data\","
        end
        data = data.chop
        data += '}'

        # Send data using REST api
        http.put("#{entry.path}",data,header)

        sleep(entry.delay) unless entry.delay==0
      end
    end
  rescue Timeout::Error => e
    Utils::LOG.info(entry){"Flood closed after #{entry.duration}s"}
  ensure
    http.delete("#{entry.path}")
  end

  def run_ssh_command(entry)
    machine = @cluster.machines.select{|m| m.hostname==entry.machine}.first
    params = "-t -i #{@config_hash['key-pair']}"
    send_cmd = "#{entry.command}"
    ssh_command(machine,send_cmd,'core',params)
  end

  def run_stress(entry)

    machine = @cluster.machines.select{|m| m.hostname==entry.machine}.first
    raise IOError.new("Unknown machine #{entry.machine} in cluster") if machine.nil?

    cpu = entry.stress_params['cpu']
    io = entry.stress_params['io']
    vm = entry.stress_params['vm']
    vm_bytes = entry.stress_params['vm-bytes']
    hdd = entry.stress_params['hdd']

    params = "-i #{@config_hash['key-pair']}"
    copy_file_to_machine(machine,@stress_location,'core',params)
    stress_cmd = "stress --cpu #{cpu} --io #{io} --vm #{vm} --vm-bytes #{vm_bytes} --vm-hang 0 --hdd #{hdd} --timeout #{entry.duration}s"
    send_cmd = "nohup ./#{stress_cmd} > /dev/null 2>&1 &"
    ssh_command(machine,send_cmd,'core',params)
  end

  # Copies a given file to a cluster machine using scp with login credentials
  def copy_file_to_machine(machine,file,user='',params='')
    ip = (@config_hash['ssh-ip']=='private'? machine.private_ipv4 : machine.public_ipv4)
    login = ''+ip
    login="#{user}@#{login}" unless user==''

    # disable stricthostcheck
    params+=' -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no'

    exec = "scp #{params} #{file} #{login}:~"
    puts "Executing command: #{exec}"
    `#{exec}`
  end

  # Sends command to a cluster machine using ssh with login credentials
  def ssh_command(machine,command, user='', params='')
    login = (@config_hash['ssh-ip']=='private'? machine.private_ipv4 : machine.public_ipv4)
    login="#{user}@#{login}" unless user==''

    # disable stricthostcheck
    params+=' -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no'

    exec = "ssh #{params} #{login} '#{command}' < /dev/null"
    puts "Executing command: #{exec}"
    `#{exec}`
  end


end