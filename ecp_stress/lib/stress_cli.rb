require 'commander'
require 'yaml'

class Stress_Cli
  include Commander::Methods

  STRESS_LOCATION='/home/stuebenc/stress'

  def run
    program :version, '0.0.1'
    program :description, 'ecp_stress'

    command :start do |c|
      c.syntax = 'ecp_stress start'
      c.option '--cluster-file STRING', String, 'Reads a cluster-file'
      c.option '--credential-file STRING', String, 'ecp_deployer credentials'
      c.option '--schedule-file STRING', String, 'stress schedule'
      c.summary = 'starts stress on deployed cluster'
      c.description = 'Starts stress schedule defined by json.'
      c.example 'example:', 'ecp_deploy start --cluster-file test.yml --credential-file creds.json'

      c.action do |args, options|

        raise IOError.new('No cluster file given') if options.cluster_file.nil?
        raise IOError.new('No schedule file given') if options.schedule_file.nil?

        cluster = YAML::load(File.open(options.cluster_file))
        credentials = options.credential_file

        sched = Scheduler.new(cluster,credentials)
        sched.stress_location = STRESS_LOCATION

        sched.start(options.schedule_file)

      end
    end
    run!
  end
end