# Scheduler classes
require_relative 'ecp_stress/scheduler/scheduler'

## Utility class for static functions
class Utils
  @verbose = true
  @log_file='stress.log'

  LOG=ScaleLogger.new

  stdout_log = Logger.new($stdout)
  file_log = Logger.new(@log_file)

  loglevel = Logger::INFO
  loglevel = Logger::DEBUG if @verbose

  stdout_log.level = loglevel
  file_log.level = loglevel

  LOG.addlogger(stdout_log)
  LOG.addlogger(file_log)

end