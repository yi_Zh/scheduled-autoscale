require 'commander'
require 'thread'
require_relative 'ecp_autoscale/utils/config_parser'

class AutoScaleCli
  include Commander::Methods

  AUTOSCALE_DEFAULT='../config/monitor.conf'
  AUTOSCALE_CONFIG='autoscale.conf'

  def initialize
    @verbose = false
    @config_hash = nil
  end

  def read_config(config_file = AUTOSCALE_CONFIG)
    file = File.join(Dir.pwd,config_file)
    file = File.join(File.dirname(File.expand_path(__FILE__)), AUTOSCALE_DEFAULT) unless File.exist?(file)
    r = ConfigParser.new file
    @config_hash = r.config_hash

    unless @config_hash['general-verbose'].nil?
      @verbose = @config_hash['general-verbose'].to_b
    end

    Utils::LOG.log_level(@verbose ? Logger::DEBUG : Logger::INFO)

  end

  def run
    program :version, '0.0.1'
    program :description, 'ecp_autoscale'

    begin
      global_option('-c', '--config FILE', 'Load config file for monitor configuration') do |file|
        read_config(file)
      end
      global_option('--verbose'){@verbose = true}

      command :start do |c|
        c.syntax = 'ecp_autoscale start'
        c.option '--cluster-file STRING', String, 'Reads a cluster-file'
        c.option '--credential-file STRING', String, 'ecp_deployer credentials'
        c.option '--metric-file STRING', String, 'Metric description in JSON'
        c.option '--agent-file STRING', String, 'Agent description in JSON'
        c.option '--scheduler STRING', String, 'scheduling algorithm'
        c.summary = 'starts monitoring of deployed cluster'
        c.description = 'Starts monitoring process of a deployed cluster. The cluster must be deployed using the ecp_deployer.'
        c.example 'example:', 'ecp_autoscale start --scheduler round-robin --credential-file creds.json --cluster-file test.yml'

        c.action do |args, options|

          raise IOError.new('Cannot start monitoring without cluster-file') if options.cluster_file==nil || options.cluster_file==''
          raise IOError.new('Cannot update cluster without credentials') if options.credential_file==nil || options.credential_file==''

          read_config if @config_hash.nil?

          @base_cluster = YAML::load(File.open(options.cluster_file))

          Utils::LOG.debug(self){"Reading cluster-file: #{options.cluster_file}"}

          ## Create elements for MAPE cycle
          executor = Executor.new(@config_hash)
          executor.credential_file = options.credential_file
          executor.agent_file = options.agent_file
          executor.load_cluster(options.cluster_file)

          planner = Planner.new(executor,@config_hash)
          planner.scheduler=options.scheduler unless options.scheduler.nil?

          analyser = Analyser.new(planner,@config_hash)

          monitor = Monitor.new(analyser,executor,@config_hash)
          monitor.cluster_file = options.cluster_file
          monitor.metric_file = options.metric_file
          monitor.load_cluster


          Thread.abort_on_exception=true

          ## Starting monitor loop
          Utils::LOG.info(self){'Starting monitor mainloop thread'}
          t1 = Thread.new{monitor.main_loop}

          ## Starting execute loop
          Utils::LOG.info(self){'Starting planner execute thread'}
          t2 = Thread.new{planner.execute_loop}

          ## Start agent runner
          Utils::LOG.info(self){'Starting agent runner thread'}
          t3 = Thread.new{planner.agent_runner}

          Utils::LOG.info(self){'Starting user input thread'}
          t4 = Thread.new{read_input(monitor,planner)}

          t4.join
          Utils::LOG.debug(self){'User input thread terminated. Waiting for agent runner thread.'}
          t3.join
          Utils::LOG.debug(self){'Agent runner thread terminated. Waiting for planner execute thread.'}
          t2.join
          Utils::LOG.debug(self){'Planner execute thread terminated. Waiting for monitor mainloop thread.'}
          t1.join
          Utils::LOG.debug(self){'Monitor mainloop thread terminated.'}
          Utils::LOG.info(self){'All threads terminated. Closing program.'}
        end
      end
      command :debug do |c|
        c.syntax = 'ecp_autoscale debug'
        c.option '--cluster-file STRING', String, 'Reads a cluster-file'
        c.option '--credential-file STRING', String, 'ecp_deployer credentials'
        c.summary = 'debugs monitoring of deployed cluster'
        c.description = 'Debugs monitoring process of a deployed cluster. The cluster must be deployed using the ecp_deployer.'
        c.example 'example:', 'ecp_autoscale debug --credential-file creds.json --cluster-file test.yml'



        c.action do |args,options|

          read_config if @config_hash.nil?

          Utils::LOG.debug(self){"Debug message shown!"}
          Utils::LOG.info(self){"Info level shown!"}


          ## Create elements for MAPE cycle
          #executor = Executor.new(@config_hash)
          #executor.credential_file = options.credential_file
          #executor.load_cluster(options.cluster_file)
          #planner = Planner.new(executor,@config_hash)
          #analyser = Analyser.new(planner,@config_hash)
          #monitor = Monitor.new(analyser,executor,@config_hash)
          #monitor.load_cluster(options.cluster_file)
          #monitor.config_hash = @config_hash
          #monitor.metric_init
          #monitor.debug(options.cluster_file)
          #read_config
          #puts @config_hash
        end
      end
      command :agentrunner do |c|
        c.syntax = 'ecp_autoscale agentrunner'
        c.option '--cluster-file STRING', String, 'Reads a cluster-file'
        c.option '--credential-file STRING', String, 'ecp_deployer credentials'
        c.option '--agent-file STRING', String, 'Agent description in JSON'
        c.summary = 'starts agentrunner process on cluster'
        c.description = 'Install and configure all agents on a cluster machines, without autoscaling.'
        c.example 'example:', 'ecp_autoscale agentrunner --credential-file creds.json --cluster-file test.yml'

        c.action do |args,options|
          read_config if @config_hash.nil?
          executor = Executor.new(@config_hash)
          executor.credential_file = options.credential_file
          executor.agent_file = options.agent_file
          executor.load_cluster(options.cluster_file)

          planner = Planner.new(executor,@config_hash)
          planner.scheduler=options.scheduler unless options.scheduler.nil?

          analyser = Analyser.new(planner,@config_hash)
          monitor = Monitor.new(analyser,executor,@config_hash)

          Thread.abort_on_exception=true

          ## Start agent runner
          Utils::LOG.info(self){'Starting agent runner thread'}
          t1 = Thread.new{planner.agent_runner}

          Utils::LOG.info(self){'Starting user input thread'}
          t2 = Thread.new{read_input(monitor,planner)}

          t2.join
          Utils::LOG.info(self){'Input thread terminated. Waiting for agent runner'}
          t1.join
          Utils::LOG.info(self){'Agent runner terminated. Shutting down programm.'}



        end
      end
    rescue IOError => e
      Cli::LOG.fatal(self) { "Error: #{e.inspect}" }
    end
    run!
  end

  # Reads user input for control
  def read_input(monitor, planner)
    input = ""

    until input == 'e' or input == 'exit'
      input = $stdin.readline.strip.downcase
    end
    Utils::LOG.info(self){'Shutdown signal received by user'}
    monitor.running = false
    planner.running = false
  end


end