require 'elasticsearch'
require 'yaml'

require_relative 'boolean_metric'
require_relative 'numeric_metric'
require_relative 'query'

# A class that implements the monitor module of a MAPE cycle
class Monitor

  METRIC_FILE='../../../config/metrics.json'
  DEFAULT_MONITOR_INTERVAL = 10


  attr_reader :metrics
  attr_writer :config_hash, :running, :cluster_file, :metric_file

  def initialize(analyser,executer,config_hash)
    @analyser = analyser
    @executor = executer
    @config_hash = config_hash

    url = @config_hash['monitor-ip']
    raise IOError.new('No monitor ip given') if url.nil?

    @es_client = Elasticsearch::Client.new(url: url)
    @update_cluster = false
    @running = true
  end

  # Reads the cluster object from file and resets analyser
  def load_cluster
    raise IOError.new("Cluster file not set.") if @cluster_file.nil?
    @cluster = YAML::load(File.open(@cluster_file))
    @update_cluster = false
    @analyser.load_cluster(@cluster)
  end

  # Initializes metrics and query for all metrics
  def metric_init
    @metrics = []
    @query = Query.new(@config_hash['agent-time-frame'])

    if @metric_file.nil?
      file = File.join(File.dirname(File.expand_path(__FILE__)),METRIC_FILE)
    else
      file = File.join(Dir.pwd,@metric_file)
    end

    metric_hash = JSON.parse(File.read(file))
    metric_hash.each do|metric|
      metric_obj = metric['type'] == 'numeric' ? NumericMetric.new(metric) : BooleanMetric.new(metric)

      @query.create_aggregation(metric_obj)
      @metrics.push(metric_obj)
    end
    @analyser.metrics = @metrics
  end

  def generate_numeric_metric(metric)


  end

  def debug(cluster_file)
    @cluster = YAML::load(File.open(cluster_file))
    puts (@query.generate(@cluster.machines.first)).to_s.gsub('\\','')
  end

  def has_updated
    @update_cluster = true
  end

  def main_loop

    interval = @config_hash['monitor-interval'].nil? ? DEFAULT_MONITOR_INTERVAL : Utils::interval_in_seconds(@config_hash['monitor-interval'])

    # Initialize monitoring components
    metric_init
    @executor.register_monitor(self)

    while @running
      # Read cluster from file when updated
      load_cluster if @update_cluster

      # Send query for each machine and analyse the result
      @cluster.machines.each do |machine|
        response = @es_client.search(@query.generate(machine))
        @analyser.insert_result(machine,response)
      end

      @analyser.analyse(@cluster)
      sleep interval
    end
  end

end