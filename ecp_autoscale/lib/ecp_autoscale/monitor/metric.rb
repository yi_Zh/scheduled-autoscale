# Implements basic metric for elastic search
class Metric

  attr_accessor :name
  attr_reader :aggregation

  def initialize(metric_hash)
    @name = metric_hash['name']
    generate_aggs(metric_hash)
    set_metric_type(metric_hash)
  end

  # Need to be overwritten by inherited class
  def generate_aggs(_agg_conf)
    @aggregation = nil
    raise StandardError.new('Base metric can\'t be instantiated ')
  end

  # Need to be overwritten by inherited class
  def set_metric_type(_metric_hash)
    raise StandardError.new('Base metric can\'t be instantiated ')
  end

  # Analyses given a of machines and gives scaling advice
  # @param _machines Array of machines
  # @param _results Hash with key=hostname, value=monitor result
  # @return -1 scale down, 0 keep setup, 1 scale up
  def analyse(_machines,_results)
    raise StandardError.new("No analyse method given in class #{self.class}")
  end

  def to_s
    "#{self.class}:#{@name}"
  end
end