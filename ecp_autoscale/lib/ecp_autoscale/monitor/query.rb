require 'jbuilder'

# Holds Queries send to elastic search
class Query

  DEFAULT_AGENT_TIMEFRAME = "30s"

  def initialize(time_frame)
    @aggs = []
    @search_index = '*'
    @time_frame = if time_frame.nil?
                    DEFAULT_AGENT_TIMEFRAME
                  else
                    time_frame.end_with?("s") or time_frame.end_with?("m") ? time_frame : "#{time_frame}s"
                  end
  end

  # Register aggregation for metrics
  def create_aggregation(metric)
    @aggs.push(metric.aggregation)
    self
  end

  # Create json_query for aggregations
  def json_query(machine)
    tmp = ''
    @aggs.each do |a|
      tmp += "#{a},"
    end
    tmp = tmp.chop
    "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"beat.hostname\" : \"#{machine.hostname.to_s}\"}},{\"range\":{\"@timestamp\": {\"gte\":\"now-#{@time_frame}\",\"lte\":\"now\"}}}]}},\"size\":0,\"aggs\": {#{tmp}}}"
  end

  # Generate json_query for specific machine
  def generate(machine)
    {index: @search_index.to_s, body: json_query(machine)}
  end

end