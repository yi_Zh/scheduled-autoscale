require_relative 'metric'

class BooleanMetric < Metric

  def set_metric_type(metric_hash)
    checks = metric_hash['checks']
    if checks.nil?
      raise IOError.new("Metric description of #{@name} incomplete. 'checks' field missing.")
    end

    count = checks['count']
    agg_field = metric_hash['aggregations']

    unless count.nil?
      @sub_type = 'count'

      if count['min'].nil?
        @sub_type += '_max'
        @boundary = count['max']
        raise IOError.new("Count metric #{@name} no boundaries") if @boundary.nil?
      else
        @sub_type += '_min'
        @boundary = count['min']
      end
    end

    @result_field = agg_field.first['name']

  end


  def generate_aggs(metric_hash)
    tmp_str = ""

    agg_field = metric_hash['aggregations']

    if agg_field.nil?
      raise IOError.new("Metric #{@name} is missing aggregation field")
    end

    agg_field.each do |agg|
      field = agg['field']
      name = agg['name']

      if field.nil?
        raise IOError.new("Metric #{@name} is missing data-field")
      end

      if name.nil?
        raise IOError.new("Metric #{@name} is missing field-name")
      end

      range = agg['range']
      func = agg['func']

      unless range.nil?
        min = range['min']
        max = range['max']

        if min.nil? or max.nil?
          raise IOError.new("Metric description of boolean range metric #{@name} incomplete. min/max missing")
        end

        tmp_str += "\"#{name}\":{\"range\":{\"field\":\"#{field}\",\"ranges\":[{\"from\": #{min}, \"to\":#{max} }] } },"
      end

      unless func.nil?
        tmp_str += "\"#{name}\":{\"#{func}\":{\"field\": \"#{field}\"}},"
        puts tmp_str
      end

    end
    @aggregation = tmp_str.chop
  end


  # Analyses given a of machines and gives scaling advice
  # @param _machines Array of machines
  # @param _results Hash with monitor results ordered by hostname
  # @return -1 scale down, 0 keep setup, 1 scale up
  def analyse(machines, results)
    machines.each do |m|

      r = results[m.hostname]
      case @sub_type
        when 'count_min'
          c = count_result(r)
          c = 0 if c.nil?
          if c < @boundary
            Utils::LOG.debug(self){"Boundary exceeded: #{c} < #{@boundary} for machine #{m.hostname}"}
          end
        when 'count_max'
          c = count_result(r)
          c = 0 if c.nil?
          if c > @boundary
            Utils::LOG.debug(self){"Boundary exceeded: #{c} > #{@boundary} for machine #{m.hostname}"}
          end
        else
          raise IOError.new("Unknown subtype #{@sub_type} for boolean metric")
      end
    end
    0
  end

  def count_result(result)
    field_data = result['aggregations'][@result_field]
    buckets = field_data['buckets']
    buckets.nil? ? field_data['value'] : buckets.first['doc_count']
  end

end