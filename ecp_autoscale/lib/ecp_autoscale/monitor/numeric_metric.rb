require_relative 'metric'

class NumericMetric < Metric

  attr_reader :name, :result_field, :min, :max, :inverse

  def set_metric_type(metric_hash)
    checks = metric_hash['checks']
    aggs = metric_hash['aggregations']
    inverse = false

    inverse = checks['inverse'] unless checks['inverse'].nil?
    total = false

    field = aggs.reject{|entry| entry['name'].include?('total')}.first
    field = field['name']

    if !checks['has-total'].nil? and checks['has-total']
      total = aggs.select{|entry| entry['name'].include?('total')}.first
      total = total['name']
    end

    min = checks['min']
    max = checks['max']

    raise IOError.new("No numeric boundaries given for metric #{@name}") if min.nil? or max.nil?

    @inverse = inverse
    @total = total
    @result_field = field


    @min = @inverse?(1-min):min
    @max = @inverse?(1-max):max
  end

  def to_s
    add = @total?" with total: #{@total}":""
    @inverse?"InverseNumericMetric: #{@name}":"NumericMetric: #{@name}".concat(add)
  end

  def generate_aggs(metric_hash)
    tmp_str = ""
    agg_field = metric_hash['aggregations']

    if agg_field.nil?
      raise IOError.new("Metric #{@name} is missing aggregation field")
    end

    agg_field.each do |agg|
      name = agg['name']
      func = agg['func']
      field = agg['field']

      if field.nil?
        raise IOError.new("Metric #{@name} is missing data-field")
      end

      if name.nil?
        raise IOError.new("Metric #{@name} is missing field-name")
      end

      if func.nil?
        raise IOError.new("Metric #{@name} is missing function")
      end

      tmp_str += "\"#{name}\":{\"#{func}\":{\"field\": \"#{field}\"}},"
    end
    @aggregation = tmp_str.chop
  end

  # Analyses given a of machines and gives scaling advice
  # @param machines Array of machines
  # @param results Hash with key=hostname, value=monitor result
  # @return negative number scale down, zero keep setup, positive number scale up
  def analyse(machines,results)
    val = 0
    machines.each do |machine|

      host_result = results[machine.hostname]
      fields = host_result['aggregations']

      data = fields[@result_field]

      total_data = nil
      total_data = fields[@total] if @total

      tmp_val = data['value']
      tmp_val = tmp_val/total_data['value'] unless (total_data.nil? or total_data['value'].nil? or total_data['value']==0)
      unless tmp_val=='nil' or tmp_val.nil?
        tmp_val = (@inverse?(1-tmp_val):tmp_val)
        val += tmp_val
      end
    end
    div = (machines.count)
    val /= div unless val.nil? or div==0

    Utils::LOG.debug(self){"Current numeric metric #{@name} is: #{val}"}

    ret = 0
    if val < @min
      ret = val<@min/2?-2:-1
      Utils::LOG.debug(self){"Role: #{machines.first.role} #{@result_field} average < #{@min}. Advice: Shutdown #{ret} server(s)"}
    end

    if val > @max
      ret = (val>[@max+(1-@max)/2, @max*2 ].min) ? 2 : 1
      Utils::LOG.debug(self){"Role: #{machines.first.role} #{@result_field} average > #{@max}. Advice: Start #{ret} server(s)"}
    end
    ret
  end
end