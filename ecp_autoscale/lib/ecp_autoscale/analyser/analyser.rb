# A class that implements the analyse module of a MAPE cycle
class Analyser

  attr_accessor :metrics

  IGNORED_ROLES = ['master']
  MIN_ROLE_NUMBER = 2
  MAX_ROLE_NUMBER = 5
  DEFAULT_TIMEOUT = 300

  def initialize(planner, config_hash)
    @planner = planner
    @config_hash = config_hash
    @metrics = []
    @machine_modifier = {}
    @results = {}
    @start = true
  end

  def load_cluster(cluster)
    @first_seen = {}
    @cluster = cluster
    @machine_modifier.clear
    @planner.clear_queue unless greedy?


    @cluster.machines.each do |m|
      @first_seen.store(m.hostname,Time.now.to_i)
    end

    (@cluster.get_cluster_roles-IGNORED_ROLES).each do |role|
      enforce_role_boundaries(role)
    end
  end

  # Function to store result belonging to specific machine
  def insert_result(machine,result)
    Utils::LOG.debug(self){"Storing result for machine: #{machine.hostname}"}
    @results.store(machine.hostname,result)
  end

  # Check if machine sends agent data
  # @param machine Machine sending data
  # @return Machine has at least one data fields
  def has_results(machine)
    total = 0
    hits = nil

    hash = @results[machine.hostname]
    hits = hash['hits'] unless hash.nil?
    total = hits['total'] unless hits.nil?
    total > 0
  end

  # Calls InitializeAction for cluster machine
  def check_for_initialized(machine)
    if has_results(machine)
      @planner.enqueue_action(InitializeAction.new(machine))
    else
      if has_timeout(machine)
        Utils::LOG.debug(self){"Time since machine #{machine.hostname} started exceeds timeout limit"}
        @planner.enqueue_action(RestartAction.new(machine))
      end
    end
  end

  # Checks if time since machine start exceedes timeout
  def has_timeout(machine)
    timeout = @config_hash['analyser-machine-timeout'].nil? ? DEFAULT_TIMEOUT : Utils::interval_in_seconds(@config_hash['analyser-machine-timeout'])
    start_time = @first_seen[machine.hostname]
    time = Time.now.to_i
    # Utils::LOG.debug(self){"Time is: #{time}, start time of #{machine.hostname} was #{start_time}, timeout is #{timeout}"}
    (time-start_time)>timeout
  end

  def min_machines
    (@config_hash['analyser-min-machine'].nil? ? MIN_ROLE_NUMBER : @config_hash['analyser-min-machine']).to_i
  end

  def max_machines
    (@config_hash['analyser-max-machine'].nil? ? MAX_ROLE_NUMBER : @config_hash['analyser-max-machine']).to_i
  end

  def greedy?
    @config_hash['analyser-greedy'].nil? ? false : @config_hash['analyser-greedy'].to_b
  end

  def enforce_role_boundaries(role)

    machines = @cluster.get_machines_by_role(role)

    machine_numer = machines.count

    if machine_numer < min_machines
      Utils::LOG.debug(self){"Role #{machines.first.role} out of boundaries. Starting #{min_machines-machine_numer} server"}
      scale_up(machines,min_machines-machine_numer)
    end
    if machine_numer > max_machines
      Utils::LOG.debug(self){"Role #{machines.first.role} out of boundaries. Removing #{machine_numer-max_machines} server"}
      scale_down(machines,machine_numer-max_machines)
    end
  end

  # Analyse a machine of a cluster based on the monitor result
  # @param cluster Cluster the machine is part of
  # @param ignored_hostnames Array of hostnames to be ignored
  def analyse(cluster)

    Utils::LOG.info(self){"Analysing cluster: #{cluster.name}"}

    roles = cluster.get_cluster_roles
    roles = roles - IGNORED_ROLES

    role_scale_value = 0.0
    roles.each do |role|

      role_machines = cluster.get_machines_by_role(role)

      # Check if new machines have been initialized
      role_machines.each do |m|
        if m.initialized
          @planner.enqueue_action(RestartAction.new(m)) unless has_results(m)
        else
          check_for_initialized(m)
        end
      end

      # Discard machines not initialized
      role_machines.delete_if{|m| !m.initialized }

      # Dont scale role if not greedy and not initilized machines
      if !greedy? && cluster.machines.select{|m| !m.initialized && m.role.downcase!='master'}.count>0
        Utils::LOG.info(self){'Stopped scaling until all machines have been initialized'}
        role_machines.clear
      end

      unless role_machines.empty?
        metric_count = 0
        @metrics.each do |metric|
          metric_count+=1 unless metric.class == BooleanMetric
          ret = metric.analyse(role_machines,@results)
          if ret == 0
            Utils::LOG.debug(self){"Cluster is working within #{metric} limits"}
          else
            role_scale_value += ret
          end
        end
        role_scale_value /= metric_count unless metric_count==0
        Utils::LOG.debug(self){"Scaling with factor #{role_scale_value}"}
        scale_role(role_machines, role_scale_value)
      end
      role_scale_value = 0.0
    end

    #puts @results
    @results.clear
  end

  def scale_role(machines,scale_value, strict=false)
    value = scale_value.round

    if value==@machine_modifier[machines.first.role]
      Utils::LOG.info(self){"Role #{machines.first.role} modify already initiated with modifier #{value}."}
    else
      if value==0
        Utils::LOG.info(self){"Role #{machines.first.role} working within all limits. Scale value is #{scale_value}"}
      else
        Utils::LOG.info(self){"Modify cluster. Scale value for #{machines.first.role} is #{scale_value}"}
        if value > 0
          scale_up(machines,value, strict)
        else
          scale_down(machines,value.abs, strict)
        end
      end
      @machine_modifier[machines.first.role]=value
    end
  end


  def scale_down(machines, number=1, strict=false)
    mod_number = 1
    cluster_scale = @machine_modifier[machines.first.role]

    if !cluster_scale.nil? && cluster_scale < 0
      if number > cluster_scale.abs
        mod_number = number-cluster_scale.abs
      end
    else
      mod_number = number
    end

    rm_machines = machines.first(mod_number)

    current_machine_count = @cluster.get_machines_by_role(machines.first.role).count-mod_number

    if current_machine_count < min_machines
      if number > 1
        Utils::LOG.info(self){'Try to shutdown one Server'}
        @machine_modifier[machines.first.role] = -1
        scale_down(machines)
      else
        Utils::LOG.info(self){'Stopping shutdown due to cluster limits'}
      end
    else
      rm_machines.each do |m|
        @planner.enqueue_action(KillAction.new(m,strict))
      end
    end
  end

  def scale_up(machines, number=1, strict=false)
    mod_number = 1
    cluster_scale = @machine_modifier[machines.first.role]

    if !cluster_scale.nil? && cluster_scale > 0
      if number > cluster_scale.abs
        mod_number = number-cluster_scale.abs
      end
    else
      mod_number = number
    end

    machine = machines.first

    current_machine_count = @cluster.get_machines_by_role(machines.first.role).count+mod_number

    if current_machine_count > max_machines
      if number > 1
        Utils::LOG.info(self){'Try to start at least one machine'}
        @machine_modifier[machines.first.role] = 1
        scale_up(machines)
      else
        Utils::LOG.info(self){'Stopping starting of machines due to cluster limits'}
      end
    else
      @planner.enqueue_action(StartAction.new(machine,mod_number,strict))
    end
  end

end