class StartAction < ClusterAction
  def initialize(machine,count=1,strict=false)
    super(machine,strict)
    @count = count
  end
  def fire(executor)
    Utils::LOG.debug(self){"Fired. Duplicating machine of #{@machine.role} #{@count} times"}
    @count.times do
      executor.cluster.addMachine(duplicate_old_machine)
    end
  end
  def duplicate_old_machine
    Machine.new(@machine.provider, @machine.storage_cluster, @machine.image, @machine.flavor, @machine.role, "#{@machine.hostname}n")
  end
end