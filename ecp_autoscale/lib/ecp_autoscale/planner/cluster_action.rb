class ClusterAction
  attr_reader :machine

  def initialize(machine,strict=false)
    @machine = machine
    @strict = strict
  end

  def fire(executor)
    raise RuntimeError.new('Called non overwritten critical function')
  end
end