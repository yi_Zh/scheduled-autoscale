class RestartAction < ClusterAction
  def fire(executor)
    Utils::LOG.debug(self){"Restarting machine #{@machine.hostname}"}
    machine = executor.cluster.machines.select{|m| m.hostname==@machine.hostname}.first
    machine.private_ipv4='' unless machine.nil?
  end
end