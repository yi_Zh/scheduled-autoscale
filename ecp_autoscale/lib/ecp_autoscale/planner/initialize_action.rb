class InitializeAction < ClusterAction
  def fire(executor)
    Utils::LOG.debug(self){"Updated machine state of #{@machine.hostname}"}
    executor.cluster.set_initialized_by_ip(@machine.private_ipv4,true)
  end
end