class KillAction < ClusterAction
  def fire(executor)
    Utils::LOG.debug(self){"Fired kill action for machine #{@machine.hostname}"}
    executor.cluster.rm_machine_by_hostname(@machine.hostname)
  end
end