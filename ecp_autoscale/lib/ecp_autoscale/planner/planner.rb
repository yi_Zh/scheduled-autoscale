require 'thread'

require_relative 'cluster_action'
require_relative 'start_action'
require_relative 'kill_action'
require_relative 'initialize_action'
require_relative 'restart_action'

# A class that implements the planner module of a MAPE cycle
class Planner

  attr_reader :executor
  attr_writer :scheduler, :running

  DEFAULT_PLANNER_INTERVAL = 30

  def initialize(executor,config_hash)
    @executor = executor
    @config_hash = config_hash

    @action_queue = []

    @scheduler = 'default'

    # Secure concurrency using mutex
    @mutex = Mutex.new
    @cv = ConditionVariable.new
    @running = true
  end

  def agent_runner
    Utils::LOG.info(self){'Starting agent runner'}
    interval = @config_hash['planner-interval'].nil? ? DEFAULT_PLANNER_INTERVAL : Utils::interval_in_seconds(@config_hash['planner-interval'])
    @executor.init_agents
    Utils::LOG.debug(self){"Agent runner interval is #{interval}"}
    while @running
      # Check for running agents
      @executor.check_for_agents
      sleep interval
    end
  end

  def execute_loop

    interval = @config_hash['planner-interval'].nil? ? DEFAULT_PLANNER_INTERVAL : Utils::interval_in_seconds(@config_hash['planner-interval'])

    while @running
      force_update = false

      Utils::LOG.info(self){'Planning cluster update execution'}

      if @scheduler=='default'
        @mutex.synchronize do
          #@cv.wait(@mutex)

          force_update = !@action_queue.empty?
          Utils::LOG.debug(self){'No update actions queued'} unless force_update

          @action_queue.each do |action|
            action.fire(@executor)
          end
          @action_queue.clear
        end
        sleep interval
      end

      if @scheduler=='round-robin'
        # TODO: Implement behavior for round robin scheduling
        raise StandardError.new('Round robin scheduling detected')
      end

      # Update cluster if necessary and still running
      @executor.update_cluster if force_update && @running
    end
  end

  def clear_queue
    @mutex.synchronize do
      @action_queue.clear
    end
  end

  # Enqueues action into queue using a mutex
  # @param action Cluster-action to modify the cluster object
  def enqueue_action(action)
    @mutex.synchronize do
      Utils::LOG.debug(self){"Enqueued action: #{action}"}
      @action_queue.push(action) unless action_already_in_queue(action)
      #@cv.signal
    end
  end

  # Checks action queue for action
  def action_already_in_queue(action)
    ret = false
    @action_queue.select{|a| a.class == action.class}.each do |entry|
      ret |= entry.machine == action.machine
    end
    Utils::LOG.debug(self){"Action #{action} for #{action.machine.hostname} already in queue"} if ret
    ret
  end
end