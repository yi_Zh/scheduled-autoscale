class Agent
  attr_accessor :name, :url, :binary, :config
  def initialize(name,url,binary,config)
    @name = name
    @url = url
    @binary = binary
    @config = config
  end
end