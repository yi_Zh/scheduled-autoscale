require 'json'
require 'pry'
require 'pry-nav'

require_relative 'agent'

# A class that implements the executor module of a MAPE cycle
class Executor

  CONFIG_PATH='../../../config'
  AGENT_FILE='../../../config/agents.json'
  TEMP_CONF_FILE='tmp_conf.yml'
  DEFAULT_SSH_TIMEOUT=5
  USER='core'

  attr_reader :recently_updated
  attr_accessor :cluster, :credential_file, :agent_file

  def initialize(config_hash)
    @agents = []
    @recently_updated = []
    @config_hash = config_hash
  end

  # Creates cluster object from cluster_file
  def load_cluster(cluster_file)
    @cluster = YAML.load(File.open(cluster_file))
    @cluster_file=cluster_file
    @monitor.cluster_file = cluster_file unless @monitor.nil?
  end

  # Checks for running agents on cluster machines. 
  # When agent is missing on a machine, restart the agent
  def check_for_agents
    Utils::LOG.info(self){'Checking agents on cluster machines.'}
    @cluster.machines.each do |machine|
      @agents.each do |agent|
        command = 'ps aux'
        params = "-i #{@config_hash['ssh-key-pair']}"
        status = ssh_command(machine,command,USER,params)
        agent_running = status.include?("./#{agent.binary}")
        Utils::LOG.debug(self){ "Agent #{agent.name} running on #{machine.hostname}: #{agent_running}" }
        start_agent(machine,agent) unless agent_running
      end
    end
  end

  # Initializes all agents on cluster machines.
  def init_agents
    Utils::LOG.debug(self){'Parsing agent file'}

    if @agent_file.nil?
      file = File.join(File.dirname(File.expand_path(__FILE__)),AGENT_FILE)
    else
      file = File.join(Dir.pwd,@agent_file)
    end

    agent_hash = JSON.parse(File.read(file))
    agent_hash.each do|agent|
      @agents.push(Agent.new(agent['name'],agent['url'],agent['binary'],agent['config']))
    end
    check_for_agents
  end

  # Starts given agent on a cluster machine
  def start_agent(machine,agent)

    Utils::LOG.debug(self){ "Starting agent #{agent.name} on #{machine.hostname}" }

    # Prepare ssh connection
    ssh_params = "-i #{@config_hash['ssh-key-pair']}"

    # Create agent config file from template
    agent_template=File.read(File.join(File.dirname(File.expand_path(__FILE__)),agent.config))

    File.open(TEMP_CONF_FILE, 'w') { |file|
      file.write(agent_template.gsub('<monitor-ip>',@config_hash['monitor-ip']))
    }
    Utils::LOG.debug(self){"Writing config for #{agent} into #{TEMP_CONF_FILE}"}

    # Copy metricbeat config to cluster machine
    copy_file_to_machine(machine, TEMP_CONF_FILE, USER, ssh_params)
    File.delete(TEMP_CONF_FILE)

    # Download and execute agent binaries
    agent_file = agent.url.rpartition('/')[2]
    local_config = agent.config.rpartition('/')[2]
    agent_dir = agent_file.gsub('.tar.gz','')

    command = "sudo wget #{agent.url};"
    command += "sudo tar -xf #{agent_file};"
    command += "sudo chown root:root #{TEMP_CONF_FILE};"
    command += "sudo mv #{TEMP_CONF_FILE} #{agent_dir}/#{local_config};"
    command += "cd #{agent_dir};"
    command += "sudo nohup ./#{agent.binary} #{local_config} > /dev/null 2>&1 &"

    ssh_command(machine,command,USER,ssh_params)
  end

  # Copies a given file to a cluster machine using scp with login credentials
  def copy_file_to_machine(machine,file,user='',params='')
    ip = (@config_hash['ssh-ip-field']=='private'? machine.private_ipv4 : machine.public_ipv4)
    login = ''+ip
    login="#{user}@#{login}" unless user==''

    # Timeout so ssh doenst get stuck on password
    timeout = @config_hash['ssh-timeout'].nil? ? DEFAULT_SSH_TIMEOUT : @config_hash['ssh-timeout']

    # disable stricthostcheck
    params+=" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no"

    exec = "timeout -s 9 #{timeout} scp #{params} #{file} #{login}:~"
    Utils::LOG.debug(self){"Executing command: #{exec}"}
    `#{exec}`
  end

  # Sends command to a cluster machine using ssh with login credentials
  def ssh_command(machine,command, user='', params='')
    login = (@config_hash['ssh-ip-field']=='private'? machine.private_ipv4 : machine.public_ipv4)
    login="#{user}@#{login}" unless user==''

    # Timeout so ssh doenst get stuck on password
    timeout = @config_hash['ssh-timeout'].nil? ? DEFAULT_SSH_TIMEOUT : @config_hash['ssh-timeout']

    # disable stricthostcheck
    params+=" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no"

    exec = "timeout -s 9 #{timeout} ssh #{params} #{login} '#{command}' < /dev/null"
    Utils::LOG.debug(self){"Executing command: #{exec}"}
    `#{exec}`
  end

  # Running cluster update using ecp_deploy
  def update_cluster

    #binding.pry
    # Saving desired cluster configuration
    tmp_name="tmp_#{@cluster.name}.yml".gsub(" ","").downcase
    safe_cluster(tmp_name)

    # Running update
    Utils::LOG.info(self){'Starting cluster-update'}
    exec="ecp_deploy update --credentials #{@credential_file} --description #{tmp_name} --status #{@cluster_file}"

    Utils::LOG.debug(self){"Running command: #{exec}"}

    `#{exec}`


    Utils::LOG.debug(self){'Cluster updated. Cleaning up files.'}

    # Clean up
    #File.delete(tmp_name)

    Utils::LOG.info(self){'Waiting for new server to arrive'}

    # Loading updated cluster
    load_cluster(@cluster_file)

    @monitor.has_updated
  end

  # Saves a cluster onto disk
  def safe_cluster(name)
    File.open(name, 'w') { |file|
      file.write(YAML.dump(@cluster))
    }
  end

  def register_monitor(monitor)
    @monitor = monitor
  end

end