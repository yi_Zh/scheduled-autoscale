class ConfigParser

  attr_accessor :config_hash

  def initialize(config_file)
    @config_hash = Hash.new
    if File.exist?(config_file)
      parse_config(config_file)
    else
      file = File.join(File.dirname(File.expand_path(__FILE__)), config_file)
      parse_config_path file
    end
  end

  # recursive search for config files
  def parse_config_path(path)
    if File.directory?(path)
      Dir.foreach(path) do |file|
        parse_config_path("#{path}/#{file}") unless (file.to_s == '.' or file.to_s == '..')
      end
    else
      parse_config(path)
    end
  end

  # parse config file into config hash
  def parse_config(file)
    if file.to_s.end_with?('.conf')
      file = File.read file
      prefix = ''
      file.each_line do |line|
        line_=line.chomp
        if line_[0] == '['
          prefix = line_.gsub('[','').gsub(']','').downcase + "-"
        else
          @config_hash[prefix + line_.split('=')[0]] = line_.split('=')[1] unless (line_[0]=='#'||line_=='')
        end
      end
    end
  end
end