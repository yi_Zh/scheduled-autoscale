class ScaleLogger

  # Define loggers to use
  def initialize *logs
    @logs = logs
  end

  def addlogger *logs
    logs.each do |log|
      @logs.push(log)
    end
  end

  def log_level(level)
    @logs.each do |log|
      log.level = level
    end
  end

  # Print into all loggers
  %w(debug info warn error fatal).each do |lvl|
    define_method(lvl) do |progname, &block|
      @logs.each { |l| l.method(lvl).call(progname, &block) }

    end
    define_method("#{lvl}?") do
      @logs.collect { |log| log.method("#{lvl}?").call }
    end
  end
end
