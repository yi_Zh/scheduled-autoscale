## Setup data structures and helper functions
require_relative 'ecp_autoscale/utils/scale_logger'

## MAPE components
require_relative 'ecp_autoscale/monitor/monitor'
require_relative 'ecp_autoscale/analyser/analyser'
require_relative 'ecp_autoscale/planner/planner'
require_relative 'ecp_autoscale/executor/executor'


## Utility class for static functions
class Utils
  @verbose = true
  @log_file='autoscale.log'

  LOG=ScaleLogger.new

  stdout_log = Logger.new($stdout)
  file_log = Logger.new(@log_file)

  loglevel = Logger::INFO
  loglevel = Logger::DEBUG if @verbose

  stdout_log.level = loglevel
  file_log.level = loglevel

  LOG.addlogger(stdout_log)
  LOG.addlogger(file_log)

  def self.interval_in_seconds(interval="")
    factor = 1
    if interval.end_with?("m")
      factor=60
    end

    while interval.end_with?("[a-zA-Z]")
      interval.chop
    end
    interval.to_i * factor
  end

end

class String
  def to_b
    self == 'true'
  end
end