Gem::Specification.new do |s|
  s.name        = 'ecp_autoscale'
  s.version     = '0.0.1'
  s.licenses    = ['MIT']
  s.summary     = 'Elastic container platform autoscale module'
  s.description = 'This module uses the ecp_deployer module to automatically scale deployed container platforms'
  s.authors     = ['Christian-Friedrich Stüben']
  s.email       = 'christian-friedrich.stueben@stud.fh-luebeck.de'
  s.files       = %w[
                  config/monitor.conf
                  config/agents.json
                  config/metrics.json
                  lib/ecp_autoscale/monitor/monitor.rb
                  lib/ecp_autoscale/monitor/metric.rb
                  lib/ecp_autoscale/monitor/query.rb
                  lib/ecp_autoscale/monitor/numeric_metric.rb
                  lib/ecp_autoscale/monitor/boolean_metric.rb
                  lib/ecp_autoscale/analyser/analyser.rb
                  lib/ecp_autoscale/planner/planner.rb
                  lib/ecp_autoscale/planner/cluster_action.rb
                  lib/ecp_autoscale/planner/kill_action.rb
                  lib/ecp_autoscale/planner/start_action.rb
                  lib/ecp_autoscale/planner/initialize_action.rb
                  lib/ecp_autoscale/planner/restart_action.rb
                  lib/ecp_autoscale/executor/executor.rb
                  lib/ecp_autoscale/executor/templates/metricbeat.yml
                  lib/ecp_autoscale/executor/templates/heartbeat.yml
                  lib/ecp_autoscale/executor/templates/packetbeat.yml
                  lib/ecp_autoscale/executor/agent.rb
                  lib/ecp_autoscale/utils/scale_logger.rb
                  lib/ecp_autoscale/utils/config_parser.rb
                  lib/ecp_autoscale.rb
                  lib/auto_scale_cli.rb
                  ]
  s.homepage    = 'https://rubygems.org/gems/example'
  s.executables = [ 'ecp_autoscale' ]
end

